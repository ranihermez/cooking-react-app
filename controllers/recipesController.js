import Recipe from "../models/Recipe.js";
import { StatusCodes } from "http-status-codes";
import { BadRequestError, NotFoundError } from "../errors/index.js";
import checkPermissions from "../utils/checkPermissions.js";
import mongoose from "mongoose";
import moment from "moment";

const createRecipe = async (req, res) => {
  const {
    title,
    timeIntotal,
    workingHours,
    numberOfPeople,
    ingredients,
    courseOfAction,
    imageUrl,
  } = req.body;

  if (
    !title ||
    !timeIntotal ||
    !workingHours ||
    !numberOfPeople ||
    !ingredients ||
    !courseOfAction ||
    !imageUrl
  ) {
    throw new BadRequestError("Angiv venligst alle værdier");
  }
  req.body.createdBy = req.user.userId;

  const recipe = await Recipe.create(req.body);
  res.status(StatusCodes.CREATED).json({ recipe });
};

const getAllRecipes = async (req, res) => {
  const { timeIntotal, numberOfPeople, sort, search } = req.query;

  const queryObject = {
    createdBy: req.user.userId,
  };
  // add stuff based on condition

  if (timeIntotal && timeIntotal !== "all") {
    queryObject.timeIntotal = timeIntotal;
  }
  if (numberOfPeople && numberOfPeople !== "all") {
    queryObject.numberOfPeople = numberOfPeople;
  }
  if (search) {
    queryObject.title = { $regex: search, $options: "i" };
  }
  // NO AWAIT

  let result = Recipe.find(queryObject);

  // chain sort conditions

  if (sort === "latest") {
    result = result.sort("-createdAt");
  }
  if (sort === "oldest") {
    result = result.sort("createdAt");
  }
  if (sort === "a-z") {
    result = result.sort("position");
  }
  if (sort === "z-a") {
    result = result.sort("-position");
  }

  //

  // setup pagination
  const page = Number(req.query.page) || 1;
  const limit = Number(req.query.limit) || 10;
  const skip = (page - 1) * limit;

  result = result.skip(skip).limit(limit);

  const recipes = await result;

  const totalRecipes = await Recipe.countDocuments(queryObject);
  const numOfPages = Math.ceil(totalRecipes / limit);

  res.status(StatusCodes.OK).json({ recipes, totalRecipes, numOfPages });
};

const updateRecipe = async (req, res) => {
  const { id: recipeId } = req.params;
  const {
    title,
    timeIntotal,
    workingHours,
    numberOfPeople,
    ingredients,
    courseOfAction,
    imageUrl,
  } = req.body;

  if (
    !title ||
    !timeIntotal ||
    !workingHours ||
    !numberOfPeople ||
    !ingredients ||
    !courseOfAction ||
    !imageUrl
  ) {
    throw new BadRequestError("Angiv venligst alle værdier");
  }
  const recipe = await Recipe.findOne({ _id: recipeId });

  if (!recipe) {
    throw new NotFoundError(`Ingen opskrift med id :${recipeId}`);
  }
  // check permissions

  checkPermissions(req.user, recipe.createdBy);

  const updatedRecipe = await Recipe.findOneAndUpdate(
    { _id: recipeId },
    req.body,
    {
      new: true,
      runValidators: true,
    }
  );

  res.status(StatusCodes.OK).json({ updatedRecipe });
};

const deleteRecipe = async (req, res) => {
  const { id: recipeId } = req.params;

  const recipe = await Recipe.findOne({ _id: recipeId });

  if (!recipe) {
    throw new NotFoundError(`Ingen opskrift med id :${recipeId}`);
  }

  checkPermissions(req.user, recipe.createdBy);

  await recipe.remove();

  res.status(StatusCodes.OK).json({ msg: "Succes! Opskriften er fjernet" });
};

const showStats = async (req, res) => {
  let stats = await Recipe.aggregate([
    { $match: { createdBy: mongoose.Types.ObjectId(req.user.userId) } },
    { $group: { _id: "$numberOfPeople", count: { $sum: 1 } } },
  ]);
  stats = stats.reduce((acc, curr) => {
    const { _id: title, count } = curr;
    acc[title] = count;
    return acc;
  }, {});

  const defaultStats = {
    pending: stats.pending || 0,
    interview: stats.interview || 0,
    declined: stats.declined || 0,
  };

  let monthlyApplications = await Recipe.aggregate([
    { $match: { createdBy: mongoose.Types.ObjectId(req.user.userId) } },
    {
      $group: {
        _id: { year: { $year: "$createdAt" }, month: { $month: "$createdAt" } },
        count: { $sum: 1 },
      },
    },
    { $sort: { "_id.year": -1, "_id.month": -1 } },
    { $limit: 6 },
  ]);
  monthlyApplications = monthlyApplications
    .map((item) => {
      const {
        _id: { year, month },
        count,
      } = item;
      const date = moment()
        .month(month - 1)
        .year(year)
        .format("MMM Y");
      return { date, count };
    })
    .reverse();

  res.status(StatusCodes.OK).json({ defaultStats, monthlyApplications });
};

export { createRecipe, deleteRecipe, getAllRecipes, updateRecipe, showStats };
