import Favorite from "../models/Favorite.js";
import { StatusCodes } from "http-status-codes";
import { BadRequestError, NotFoundError } from "../errors/index.js";
import checkPermissions from "../utils/checkPermissions.js";

const createFavorite = async (req, res) => {
  const userId = req.user.userId;
  const recipeId = req.body._id;
  const recipeTitle = req.body.title;

  const favoriteAlreadyExists = await Favorite.findOne({ recipeTitle });
  if (favoriteAlreadyExists) {
    throw new BadRequestError("Allerede favorit");
  }
  const favorite = await Favorite.create({
    userId,
    recipeId,
    recipeTitle,
  });
  res.status(StatusCodes.CREATED).json({ favorite });
};

const getAllFavorites = async (req, res) => {
  const { sort } = req.query;

  const queryObject = {
    createdBy: req.user.userId,
  };

  let result = Favorite.find(queryObject);

  // chain sort conditions

  if (sort === "latest") {
    result = result.sort("-createdAt");
  }
  if (sort === "oldest") {
    result = result.sort("createdAt");
  }
  if (sort === "a-z") {
    result = result.sort("position");
  }
  if (sort === "z-a") {
    result = result.sort("-position");
  }

  //

  // setup pagination
  const page = Number(req.query.page) || 1;
  const limit = Number(req.query.limit) || 10;
  const skip = (page - 1) * limit;

  result = result.skip(skip).limit(limit);

  const favorites = await result;

  res.status(StatusCodes.OK).json({ favorites });
};
const deleteFavorite = async (req, res) => {
  const { id: favoriteId } = req.params;

  const favorite = await Favorite.findOne({ _id: favoriteId });

  if (!favorite) {
    throw new NotFoundError(`Ingen favorit med id:${favoriteId}`);
  }

  checkPermissions(req.user, favorite.userId);

  await favorite.remove();

  res.status(StatusCodes.OK).json({ msg: "Succes! favorit fjernet" });
};

export { createFavorite, getAllFavorites, deleteFavorite };
