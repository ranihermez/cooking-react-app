import mongoose from 'mongoose'

const RecipeSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: [true, 'Angiv venligst titel'],
      maxlength: 50,
    },
    timeIntotal: {
      type: String,
      enum: [
        "0 min.",
        "10 min.",
        "15 min.",
        "20 min.",
        "25 min.",
        "30 min.",
        "35 min.",
        "40 min.",
        "45 min.",
        "50 min.",
        "60 min.",
        "120 min.",
      ],
      default: '0 min.',
    },
    workingHours: {
      type: String,
      enum: [
        "10 min.",
        "15 min.",
        "20 min.",
        "30 min.",
        "40 min.",
        "50 min.",
        "60 min.",
        "70 min.",
        "80 min.",
        "90 min.",
        "100 min.",
        "110 min.",
        "120 min.",
        "130 min.",
        "140 min.",
        "150 min.",
        "160 min.",
        "170 min.",
        "180 min.",
        "190 min.",
        "200 min.",
      ],
      default: '10 min.',
      required: [true, 'Angiv venligst arbejdstid'],
    },
    numberOfPeople: {
      type: String,
      enum: ['1', '2', '3', '4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'],
      default: '0',
      required: [true, 'Angiv venligst antal personer'],
    },
    ingredients: {
      type: String,
      required: [true, 'Angiv venligst ingredienser'],
    },
    courseOfAction:{
      type: String,
      required: [true, 'Giv venligst fremgangsmåde'],
    },
    imageUrl: {
      type: String,
      required: true
    },
    createdBy: {
      type: mongoose.Types.ObjectId,
      ref: 'User',
      required: [true, 'Angiv venligst bruger'],
    },
  },
  { timestamps: true }
)

export default mongoose.model('Recipe', RecipeSchema)
