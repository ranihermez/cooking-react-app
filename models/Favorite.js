import mongoose from 'mongoose'

const FavoriteSchema = new mongoose.Schema({
    userId:{
        type: mongoose.Types.ObjectId,
        ref:'User'
    },
    recipeId:{
        type: mongoose.Types.ObjectId,
        ref:'Recipe'
    },
    recipeTitle:{
        type: String
    },
})

export default mongoose.model('Favorite', FavoriteSchema)
