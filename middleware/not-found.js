const notFoundMiddleware = (req, res) =>
    res.status(404).send('Routen findes ikke')

export default notFoundMiddleware