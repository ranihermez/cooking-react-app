import {
  DISPLAY_ALERT,
  CLEAR_ALERT,
  SETUP_USER_BEGIN,
  SETUP_USER_SUCCESS,
  SETUP_USER_ERROR,
  TOGGLE_SIDEBAR,
  LOGOUT_USER,
  UPDATE_USER_BEGIN,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_ERROR,
  HANDLE_CHANGE,
  CLEAR_VALUES,
  CREATE_RECIPE_BEGIN,
  CREATE_RECIPE_SUCCESS,
  CREATE_RECIPE_ERROR,
  CREATE_FAVORITE_BEGIN,
  CREATE_FAVORITE_SUCCESS,
  CREATE_FAVORITE_ERROR,
  DELETE_FAVORITE_BEGIN,
  GET_FAVORITE_BEGIN,
  GET_FAVORITE_SUCCESS,
  GET_RECIPES_BEGIN,
  GET_RECIPES_SUCCESS,
  GET_RECIPE,
  SET_EDIT_RECIPE,
  DELETE_RECIPE_BEGIN,
  EDIT_RECIPE_BEGIN,
  EDIT_RECIPE_SUCCESS,
  EDIT_RECIPE_ERROR,
  SHOW_STATS_BEGIN,
  SHOW_STATS_SUCCESS,
  CLEAR_FILTERS,
  CHANGE_PAGE,
} from './actions'

import { initialState } from './appContext'

const reducer = (state, action) => {
  if (action.type === DISPLAY_ALERT) {
    return {
      ...state,
      showAlert: true,
      alertType: 'danger',
      alertText: 'Angiv venligst alle værdier!',
    }
  }
  if (action.type === CLEAR_ALERT) {
    return {
      ...state,
      showAlert: false,
      alertType: '',
      alertText: '',
    }
  }

  if (action.type === SETUP_USER_BEGIN) {
    return { ...state, isLoading: true }
  }
  if (action.type === SETUP_USER_SUCCESS) {
    return {
      ...state,
      isLoading: true,
      token: action.payload.token,
      user: action.payload.user,
      showAlert: true,
      alertType: 'success',
      alertText: action.payload.alertText,
    }
  }
  if (action.type === SETUP_USER_ERROR) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'danger',
      alertText: action.payload.msg,
    }
  }
  if (action.type === TOGGLE_SIDEBAR) {
    return {
      ...state,
      showSidebar: !state.showSidebar,
    }
  }
  if (action.type === LOGOUT_USER) {
    return {
      ...initialState,
      user: null,
      token: null
    }
  }
  if (action.type === UPDATE_USER_BEGIN) {
    return { ...state, isLoading: true }
  }
  if (action.type === UPDATE_USER_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      token: action.payload.token,
      user: action.payload.user,
      showAlert: true,
      alertType: 'success',
      alertText: 'Brugerprofil opdateret!',
    }
  }
  if (action.type === UPDATE_USER_ERROR) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'danger',
      alertText: action.payload.msg,
    }
  }
  if (action.type === HANDLE_CHANGE) {
    return {
      ...state,
      page: 1,
      [action.payload.name]: action.payload.value,
    }
  }
  if (action.type === CLEAR_VALUES) {
    const initialState = {
      isEditing: false,
      editRecipeId: '',
      title: '',
      workingHours: '',
      ingredients: '',
      courseOfAction:'',
      timeIntotal: '10',
      numberOfPeople: '1',
    }

    return {
      ...state,
      ...initialState,
    }
  }
  if (action.type === CREATE_RECIPE_BEGIN) {
    return { ...state, isLoading: true }
  }

  if (action.type === CREATE_RECIPE_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'success',
      alertText: 'Ny opskrift oprettet!',
    }
  }
  if (action.type === CREATE_RECIPE_ERROR) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'danger',
      alertText: action.payload.msg,
    }
  }
  if (action.type === CREATE_FAVORITE_BEGIN) {
    return { ...state, isLoading: true }
  }

  if (action.type === CREATE_FAVORITE_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'success',
      alertText: 'Opskrift tilføjet til favoritlisten!',
    }
  }
  if (action.type === CREATE_FAVORITE_ERROR) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'danger',
      alertText: 'Opskriften er allerede dit Favorit',
    }
  }
  if (action.type === GET_RECIPES_BEGIN) {
    return { ...state, isLoading: true, showAlert: false }
  }
  if (action.type === GET_FAVORITE_BEGIN) {
    return { ...state, isLoading: true, showAlert: false }
  }
  if (action.type === GET_FAVORITE_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      favorites: action.payload.favorites,
      numOfPages: action.payload.numOfPages,
    }
  }
  if (action.type === GET_RECIPES_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      recipes: action.payload.recipes,
      totalRecipes: action.payload.totalRecipes,
      numOfPages: action.payload.numOfPages,
    }
  }
  if (action.type === GET_RECIPE) {
      const recipe = state.recipes.find((recipe) => recipe._id === action.payload.id)
      const {title, timeIntotal, workingHours, numberOfPeople, ingredients, courseOfAction,imageUrl} = recipe
      return {
        ...state,
        title,
        timeIntotal,
        workingHours,
        numberOfPeople,
        ingredients,
        courseOfAction,
        imageUrl
      }
  }
  if (action.type === SET_EDIT_RECIPE) {
    const recipe = state.recipes.find((recipe) => recipe._id === action.payload.id)
    const { _id, title, timeIntotal, workingHours, numberOfPeople, ingredients, courseOfAction,imageUrl} = recipe
    return {
      ...state,
      isEditing: true,
      editRecipeId: _id,
      title,
      timeIntotal,
      workingHours,
      numberOfPeople,
      ingredients,
      courseOfAction,
      imageUrl
    }
  }
  if (action.type === DELETE_RECIPE_BEGIN) {
    return { ...state, isLoading: true }
  }
  if (action.type === DELETE_FAVORITE_BEGIN) {
    return { ...state, isLoading: true }
  }
  if (action.type === EDIT_RECIPE_BEGIN) {
    return {
      ...state,
      isLoading: true,
    }
  }
  if (action.type === EDIT_RECIPE_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'success',
      alertText: 'Opskrift opdateret!',
    }
  }
  if (action.type === EDIT_RECIPE_ERROR) {
    return {
      ...state,
      isLoading: false,
      showAlert: true,
      alertType: 'danger',
      alertText: action.payload.msg,
    }
  }
  if (action.type === SHOW_STATS_BEGIN) {
    return {
      ...state,
      isLoading: true,
      showAlert: false,
    }
  }
  if (action.type === SHOW_STATS_SUCCESS) {
    return {
      ...state,
      isLoading: false,
      stats: action.payload.stats,
      monthlyApplications: action.payload.monthlyApplications,
    }
  }
  if (action.type === CLEAR_FILTERS) {
    return {
      ...state,
      search: '',
      searchStatus: 'all',
      searchType: 'all',
      sort: 'latest',
    }
  }
  if (action.type === CHANGE_PAGE) {
    return { ...state, page: action.payload.page }
  }
  throw new Error(`ingen sådan handling : ${action.type}`)
}

export default reducer
