import React, { useReducer, useContext } from "react";

import reducer from "./reducer";
import axios from "axios";
import {
  DISPLAY_ALERT,
  CLEAR_ALERT,
  SETUP_USER_BEGIN,
  SETUP_USER_SUCCESS,
  SETUP_USER_ERROR,
  TOGGLE_SIDEBAR,
  LOGOUT_USER,
  UPDATE_USER_BEGIN,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_ERROR,
  HANDLE_CHANGE,
  CLEAR_VALUES,
  CREATE_RECIPE_BEGIN,
  CREATE_RECIPE_SUCCESS,
  CREATE_RECIPE_ERROR,
  CREATE_FAVORITE_BEGIN,
  CREATE_FAVORITE_SUCCESS,
  CREATE_FAVORITE_ERROR,
  GET_FAVORITE_BEGIN,
  GET_RECIPES_BEGIN,
  GET_RECIPES_SUCCESS,
  SET_EDIT_RECIPE,
  DELETE_RECIPE_BEGIN,
  DELETE_FAVORITE_BEGIN,
  EDIT_RECIPE_BEGIN,
  EDIT_RECIPE_SUCCESS,
  EDIT_RECIPE_ERROR,
  SHOW_STATS_BEGIN,
  SHOW_STATS_SUCCESS,
  CLEAR_FILTERS,
  CHANGE_PAGE,
  GET_RECIPE,
  GET_FAVORITE_SUCCESS,
} from "./actions";

const token = localStorage.getItem("token");
const user = localStorage.getItem("user");

const initialState = {
  isLoading: false,
  showAlert: false,
  alertText: "",
  alertType: "",
  user: user ? JSON.parse(user) : null,
  token: token,
  showSidebar: false,
  isEditing: false,
  editRecipeId: "",
  title: "",
  workingHours: "10 min.",
  workingHoursOptions: [
    "10 min.",
    "15 min.",
    "20 min.",
    "30 min.",
    "40 min.",
    "50 min.",
    "60 min.",
    "70 min.",
    "80 min.",
    "90 min.",
    "100 min.",
    "110 min.",
    "120 min.",
    "130 min.",
    "140 min.",
    "150 min.",
    "160 min.",
    "170 min.",
    "180 min.",
    "190 min.",
    "200 min.",
  ],
  ingredients: "",
  courseOfAction: "",
  imageUrl: "",
  timeIntotalOptions: [
    "0 min.",
    "10 min.",
    "15 min.",
    "20 min.",
    "25 min.",
    "30 min.",
    "35 min.",
    "40 min.",
    "45 min.",
    "50 min.",
    "60 min.",
    "120 min.",
  ],
  timeIntotal: "0 min.",
  numberOfPeopleOptions: [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
  ],
  numberOfPeople: "1",
  recipes: [],
  favorites: [],
  totalRecipes: 0,
  numOfPages: 1,
  page: 1,
  stats: {},
  search: "",
  searchStatus: "all",
  searchType: "all",
  sort: "latest",
  sortOptions: ["latest", "oldest", "a-z", "z-a"],
};

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  // axios
  const authFetch = axios.create({
    baseURL: "/api/v1",
  });
  // request

  authFetch.interceptors.request.use(
    (config) => {
      config.headers.common["Authorization"] = `Bearer ${state.token}`;
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  // response

  authFetch.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      // console.log(error.response)
      if (error.response.status === 401) {
        logoutUser();
      }
      return Promise.reject(error);
    }
  );

  const displayAlert = () => {
    dispatch({ type: DISPLAY_ALERT });
    clearAlert();
  };

  const clearAlert = () => {
    setTimeout(() => {
      dispatch({ type: CLEAR_ALERT });
    }, 3000);
  };

  const addUserToLocalStorage = ({ user, token }) => {
    localStorage.setItem("user", JSON.stringify(user));
    localStorage.setItem("token", token);
  };

  const removeUserFromLocalStorage = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
  };

  const setupUser = async ({ currentUser, endPoint, alertText }) => {
    dispatch({ type: SETUP_USER_BEGIN });
    try {
      const { data } = await axios.post(
        `/api/v1/auth/${endPoint}`,
        currentUser
      );

      const { user, token } = data;
      dispatch({
        type: SETUP_USER_SUCCESS,
        payload: { user, token, alertText },
      });
      addUserToLocalStorage({ user, token });
    } catch (error) {
      dispatch({
        type: SETUP_USER_ERROR,
        payload: { msg: error.response.data.msg },
      });
    }
    clearAlert();
  };
  const toggleSidebar = () => {
    dispatch({ type: TOGGLE_SIDEBAR });
  };

  const logoutUser = () => {
    dispatch({ type: LOGOUT_USER });
    removeUserFromLocalStorage();
  };
  const updateUser = async (currentUser) => {
    dispatch({ type: UPDATE_USER_BEGIN });
    try {
      const { data } = await authFetch.patch("/auth/updateUser", currentUser);

      const { user, token } = data;

      dispatch({
        type: UPDATE_USER_SUCCESS,
        payload: { user, token },
      });
      addUserToLocalStorage({ user, token });
    } catch (error) {
      if (error.response.status !== 401) {
        dispatch({
          type: UPDATE_USER_ERROR,
          payload: { msg: error.response.data.msg },
        });
      }
    }
    clearAlert();
  };

  const handleChange = ({ name, value }) => {
    dispatch({ type: HANDLE_CHANGE, payload: { name, value } });
  };

  const clearValues = () => {
    dispatch({ type: CLEAR_VALUES });
  };
  const createRecipe = async () => {
    dispatch({ type: CREATE_RECIPE_BEGIN });
    try {
      const {
        title,
        timeIntotal,
        workingHours,
        numberOfPeople,
        ingredients,
        courseOfAction,
        imageUrl,
      } = state;
      await authFetch.post("/recipes", {
        title,
        timeIntotal,
        workingHours,
        numberOfPeople,
        ingredients,
        courseOfAction,
        imageUrl,
      });
      dispatch({ type: CREATE_RECIPE_SUCCESS });
      dispatch({ type: CLEAR_VALUES });
    } catch (error) {
      if (error.response.status === 401) return;
      dispatch({
        type: CREATE_RECIPE_ERROR,
        payload: { msg: error.response.data.msg },
      });
    }
    clearAlert();
  };
  const createFavorite = async () => {
    dispatch({ type: CREATE_FAVORITE_BEGIN });
    try {
      const { userId, recipeId, title } = state;

      
        await authFetch.post("/favorites", {
        userId,
        recipeId,
        title,
      });
      dispatch({ type: CREATE_FAVORITE_SUCCESS });
      
    } catch (error) {
      if (error.response.status === 401) return;
      dispatch({
        type: CREATE_FAVORITE_ERROR,
        payload: { msg: error.response.data.msg },
      });
    }
    clearAlert();
  };
  const getFavorites = async () => {
    const { page, sort } = state;

    let url = `/favorites?page=${page}&sort=${sort}`;
    if (sort) {
      url = url + `&sort=${sort}`;
    }
    dispatch({ type: GET_FAVORITE_BEGIN });
    try {
      const { data } = await authFetch(url);
      const { favorites, numOfPages } = data;
      dispatch({
        type: GET_FAVORITE_SUCCESS,
        payload: {
          favorites,
          numOfPages,
        },
      });
    } catch (error) {
      logoutUser();
    }
    clearAlert();
  };
  const deleteFavorite = async (favoriteId) => {
    dispatch({ type: DELETE_FAVORITE_BEGIN});
    try {
      await authFetch.delete(`/favorites/${favoriteId}`);
      getFavorites();
    } catch (error) {
      logoutUser();
    }
  };
  const getRecipe = async (id) => {
    dispatch({ type: GET_RECIPE, payload: { id } });
  };
  const getRecipes = async () => {
    const { page, search, searchStatus, searchType, sort } = state;

    let url = `/recipes?page=${page}&timeIntotal=${searchStatus}&numberOfPeople=${searchType}&sort=${sort}`;
    if (search) {
      url = url + `&search=${search}`;
    }
    dispatch({ type: GET_RECIPES_BEGIN });
    try {
      const { data } = await authFetch(url);
      const { recipes, totalRecipes, numOfPages } = data;
      dispatch({
        type: GET_RECIPES_SUCCESS,
        payload: {
          recipes,
          totalRecipes,
          numOfPages,
        },
      });
    } catch (error) {
      logoutUser();
    }
    clearAlert();
  };
  const setEditRecipe = (id) => {
    dispatch({ type: SET_EDIT_RECIPE, payload: { id } });
  };
  const editRecipe = async () => {
    dispatch({ type: EDIT_RECIPE_BEGIN });

    try {
      const {
        title,
        timeIntotal,
        workingHours,
        numberOfPeople,
        ingredients,
        courseOfAction,
        imageUrl,
      } = state;
      await authFetch.patch(`/recipes/${state.editRecipeId}`, {
        title,
        timeIntotal,
        workingHours,
        numberOfPeople,
        ingredients,
        courseOfAction,
        imageUrl,
      });
      dispatch({ type: EDIT_RECIPE_SUCCESS });
      dispatch({ type: CLEAR_VALUES });
    } catch (error) {
      if (error.response.status === 401) return;
      dispatch({
        type: EDIT_RECIPE_ERROR,
        payload: { msg: error.response.data.msg },
      });
    }
    clearAlert();
  };
  const deleteRecipe = async (recipeId) => {
    dispatch({ type: DELETE_RECIPE_BEGIN });
    try {
      await authFetch.delete(`/recipes/${recipeId}`);
      getRecipes();
    } catch (error) {
      logoutUser();
    }
  };
  const showStats = async () => {
    dispatch({ type: SHOW_STATS_BEGIN });
    try {
      const { data } = await authFetch("/recipes/stats");
      dispatch({
        type: SHOW_STATS_SUCCESS,
        payload: {
          stats: data.defaultStats,
          monthlyApplications: data.monthlyApplications,
        },
      });
    } catch (error) {
      logoutUser();
    }
    clearAlert();
  };
  const clearFilters = () => {
    dispatch({ type: CLEAR_FILTERS });
  };
  const changePage = (page) => {
    dispatch({ type: CHANGE_PAGE, payload: { page } });
  };

  return (
    <AppContext.Provider
      value={{
        ...state,
        displayAlert,
        setupUser,
        toggleSidebar,
        logoutUser,
        updateUser,
        handleChange,
        clearValues,
        createRecipe,
        createFavorite,
        getRecipes,
        getFavorites,
        deleteFavorite,
        setEditRecipe,
        deleteRecipe,
        editRecipe,
        getRecipe,
        showStats,
        clearFilters,
        changePage,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

const useAppContext = () => {
  return useContext(AppContext);
};

export { AppProvider, initialState, useAppContext };
