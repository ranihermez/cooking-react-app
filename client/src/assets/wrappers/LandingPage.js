import styled from 'styled-components'

const Wrapper = styled.main`
nav {
    width: var(--fluid-width);
    max-width: var(--max-width);
    margin: 0 auto;
    display: flex;
    margin-top:35px;
    justify-content: center;
  }
  .page {
    min-height: calc(50vh - var(--nav-height));
    display: grid;
    align-items: center;
  }
  h1 {
    font-weight: 700;
    span {
      color: var(--primary-500);
    }
  }
  p {
    color: var(--black);
    text-align:center;
  }
  .info{
    display:flex;
    flex-direction: column; 
    gap:15px;

  }
  @media (min-width: 992px) {
    .page {
      display:flex;
      justify-content: center;
    }
  }
`
export default Wrapper
