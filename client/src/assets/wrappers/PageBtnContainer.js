import styled from 'styled-components'

const Wrapper = styled.section`
  height: 6rem;
  margin-bottom: 4rem;
  display: flex;
  align-items: center;
  justify-content: end;
  flex-wrap: wrap;
  .btn-container {
    background: #03764d80;
    border-radius: var(--borderRadius);
  }
  .pageBtn {
    background: transparent;
    border-color: transparent;
    width: 50px;
    height: 40px;
    font-weight: 700;
    font-size: 1.25rem;
    color: #fff;
    transition: var(--transition);
    border-radius: var(--borderRadius);
    cursor: pointer;
  }
  .active {
    background: var(--primary-1000);
    color: var(--white);
  }
  .prev-btn,
  .next-btn {
    width: 100px;
    height: 40px;
    background: var(--grey-1000);
    border-color: transparent;
    border-radius: var(--borderRadius);
    color: #fff;
    text-transform: capitalize;
    letter-spacing: var(--letterSpacing);
    display: flex;
    align-items: center;
    justify-content: center;
    gap: 0.5rem;
    cursor: pointer;
    transition: var(--transition);
  }
  .prev-btn:hover,
  .next-btn:hover {
    background: var(--primary-1000);
    color: var(--white);
  }
`
export default Wrapper
