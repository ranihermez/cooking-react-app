import styled from 'styled-components'

const Wrapper = styled.aside`
  @media (min-width: 992px) {
    display: none;
  }
  .sidebar-container {
    position: fixed;
    inset: 0;
    background: rgba(0, 0, 0, 0.7);
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: -1;
    opacity: 0;
    transition: var(--transition);
  }
  .show-sidebar {
    z-index: 99;
    opacity: 1;
  }
  .content {
    background: var(--primary-1000);
    width: var(--fluid-width);
    height: 95vh;
    border-radius: 25px;
    padding: 4rem 2rem;
    position: relative;
    display: flex;
    align-items: left;
    flex-direction: column;
  }
  .close-btn {
    position: absolute;
    top: 10px;
    left: 10px;
    background: transparent;
    border-color: transparent;
    font-size: 2rem;
    color: var(--white);
    cursor: pointer;
  }
  .nav-links {
    padding-top: 1rem;
    display: flex;
    flex-direction: column;
  }
  .nav-link {
    display: flex;
    align-items: center;
    color: var(--white);
    padding: 1rem;
    border-radius:25px;
    text-transform: capitalize;
    transition: var(--transition);
  }
  .nav-link:hover {
    background: #FF8730;
    color: var(--white);
  }
  .nav-link:hover .icon {
    color: var(--white);
  }
  .icon {
    font-size: 1.5rem;
    margin-right: 1rem;
    display: grid;
    place-items: center;
    transition: var(--transition);
  }
  .active {
    color: var(--white);
  }
  .active .icon {
    color: var(--white);
  }
`
export default Wrapper
