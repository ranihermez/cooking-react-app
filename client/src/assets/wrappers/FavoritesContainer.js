import styled from "styled-components";

const Wrapper = styled.section`
  margin-bottom: 90px;
    display: flex;
    flex-direction: column;
    align-items: center;

  h1 {
    font-family: "Cairo";
    color: #000;
  }
  p {
    font-family: "Cairo";
    color: rgb(0 0 0 / 50%);
    text-align: center;
  }
  .recipes {
    display: grid;
    grid-template-columns: 1fr;
    row-gap: 1rem;
  }
  @media (min-width: 992px) {
    .recipes {
      display: grid;
      grid-template-columns: auto auto auto auto;
      grid-gap: 10px;
    }
  }
`;

export default Wrapper;
