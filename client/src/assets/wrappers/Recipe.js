import styled from "styled-components";

const Wrapper = styled.article`
  background: #03764d;
  border-radius: 25px;
  display: grid;
  grid-template-rows: 1fr auto;
  box-shadow: 4px 4px 15px 5px rgba(0, 0, 0, 0.35);
  margin-top: 10px;
  header {
    color: #fff;
    text-align: center;
    h5 {
      letter-spacing: 0;
    }
  }
  .main-icon {
    width: 60px;
    height: 60px;
    display: grid;
    place-items: center;
    background: var(--primary-500);
    border-radius: var(--borderRadius);
    font-size: 1.5rem;
    font-weight: 700;
    text-transform: uppercase;
    color: var(--white);
    margin-right: 2rem;
  }
  .red-icon{
    color:#ff8730;
  }
  .white-icon{
    color:#fff;
  }
  .info {
    h5 {
      margin-bottom: 0.25rem;
    }
    h1 {
      margin: 5px;
    }
    p {
      margin: 0;
      text-transform: capitalize;
      color: #fff;
      letter-spacing: var(--letterSpacing);
      text-align: left;
    }
  }
  .pending {
    background: #fcefc7;
    color: #e9b949;
  }
  .interview {
    background: #e0e8f9;
    color: #647acb;
  }
  .declined {
    color: #d66a6a;
    background: #ffeeee;
  }
  .content {
    padding: 1rem 1.5rem;
  }
  .icon{
    position: relative;
    top: 2px;
  }
  .content-center {
    display: flex;
    justify-content: space-around;
    margin-bottom: 7px;

    @media (min-width: 576px) {
      grid-template-columns: 1fr 1fr;
    }
    @media (min-width: 992px) {
      grid-template-columns: 1fr;
    }

  }
  .se-recipe {
    flex: 0 0 auto;
    display: flex;
    align-items: center;
    justify-content: space-around;
  }
  .se-p {
    font-size: 12px;
  }
  .ing-se-des {
    @media (min-width: 390px) {
      width: 160px;
    }
    @media (min-width: 1120px) {
      width: 390px;
    }
  }
  .ing-se {
    display: flex;
    flex-direction: row;
    margin-top: 20px;
    justify-content: space-around;
  }
 
  .status {
    border-radius: var(--borderRadius);
    text-transform: capitalize;
    letter-spacing: var(--letterSpacing);
    text-align: center;
    width: 100px;
    height: 30px;
  }
  footer {
    margin-top: 1rem;
    display: flex;
    justify-content: center;
  }
  .edit-btn,
  .delete-btn {
    letter-spacing: var(--letterSpacing);
    cursor: pointer;
    height: 30px;
  }
  .edit-btn {
    color: var(--green-dark);
    background: var(--green-light);
    margin-right: 0.5rem;
  }
  .delete-btn {
    color: var(--red-dark);
    background: var(--red-light);
  }
  .se-btn {
    color: var(--white);
    background: #1f9c70;
  }
  &:hover .actions {
    visibility: visible;
  }
`;

export default Wrapper;
