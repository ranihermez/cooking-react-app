import styled from "styled-components";

const Wrapper = styled.section`
  h1 {
    font-family: "Cairo";
    color: #000;
  }
`;

export default Wrapper;
