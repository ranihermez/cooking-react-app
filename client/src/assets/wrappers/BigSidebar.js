import styled from 'styled-components'

const Wrapper = styled.aside`
  display: none;
  @media (min-width: 992px) {
    display: block;
    box-shadow: 1px 0px 0px 0px rgba(0, 0, 0, 0.1);
    .sidebar-container {
      background: var(--primary-1000);
      min-height: 100vh;
      height: 100%;
      width: 250px;
      margin-left: -250px;
      transition: var(--transition);
    }
    .content {
      position: sticky;
      top: 0;
    }
    .show-sidebar {
      margin-left: 0;
    }
    header {
      display: flex;
      align-items: center;
      padding-left: 2.5rem;
    }
    header img{
      height:8rem;
      align-items: center;
      padding-left: 2.5rem;
    }
    .nav-links {
      padding-top: 2rem;
      display: flex;
      flex-direction: column;
    }
    .nav-link {
      display: flex;
      align-items: center;
      color: var(--white);
      padding: 1rem;
      border-radius:25px;
      text-transform: capitalize;
      transition: var(--transition);
    }
    .nav-link:hover {
      background: #FF8730;
      padding-left: 3rem;
      color: var(--white);
    }
    .nav-link:hover .icon {
      color: var(--white);
    }
    .icon {

      font-size: 1.5rem;
      margin-right: 1rem;
      display: grid;
      place-items: center;
      transition: var(--transition);
    }
    .active {
      color: var(--white);
    }
    .active .icon {
      color: var(--white);
    }
  }
`
export default Wrapper
