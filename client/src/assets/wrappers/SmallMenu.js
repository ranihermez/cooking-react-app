import styled from 'styled-components'

const DisplayNone = styled.div`
@media (min-width: 391px) {
  display: none;
}
`;
const Rect = styled.div`
    top: 35px;
    height: 79px;
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    position: absolute;
    background-color: rgba(255,255,255,1);
    width: 390px;
    left: -20px;
    box-shadow: 0px 0px 4px 0.5px rgb(0 0 0);
`;

const Rect2 = styled.div`
  top: 0px;
  width: 60px;
  height: 60px;
  position: absolute;
  border-radius: 100px;
  left: 143px;
  background-color: rgba(255,255,255,1);
  flex-direction: column;
  display: flex;
  box-shadow: 0px 0px 4px 0.5px rgba(0,0,0,1);
`;

const RectStack = styled.div`
  position: fixed;
  bottom: 113px;
`;
const Flex = styled.div`
display: flex;
justify-content: center;
gap: 60px;
margin-top:20px;
`;
const FlexWrapper = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
font-size: 0.8em;
`; 

export {Rect, Rect2,RectStack,Flex,FlexWrapper, DisplayNone}


