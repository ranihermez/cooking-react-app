import styled from "styled-components";

const Wrapper = styled.section`
  margin-bottom: 90px;
  h2 {
    text-transform: none;
  }
  & > h5 {
    font-weight: 700;
  }
  .recipes {
    display: grid;
    grid-template-columns: 1fr;
    row-gap: 1rem;
  }
  @media (min-width: 992px) {
    .recipes {
      display: grid;
      grid-template-columns: auto auto auto auto;
      grid-gap: 10px;
    }
  }
`;
export default Wrapper;
