import styled from "styled-components";

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: left;
  
  .se-btn {
    color: var(--white);
    background: #1f9c70;
    margin-bottom: 15px;
  }
  h1 {
    font-family: "Cairo";
    color: #000;
  }
  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr;
    column-gap: 1rem;
  }
  @media (min-width: 1120px) {
    grid-template-columns: 1fr 1fr 1fr;
    column-gap: 1rem;
  }
`;
export default Wrapper;
