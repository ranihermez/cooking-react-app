import { BsFillCalendar2EventFill, BsFillPlusCircleFill, BsHeartFill} from 'react-icons/bs'
import { MdOutlineRestaurantMenu } from 'react-icons/md'
import { FaHome, FaBuffer, FaUserCircle, FaClipboardList} from 'react-icons/fa'

const links = [
  { id: 1, text: 'Hjem', path: '/', icon: <FaHome /> },
  { id: 2, text: 'Opskrifter', path: '/all-recipes', icon: <FaBuffer/> },
  { id: 3, text: 'Menu', path: '/', icon: <MdOutlineRestaurantMenu /> },
  { id: 4, text: 'Madplan', path: '/', icon: <BsFillCalendar2EventFill /> },
  { id: 5, text: 'Opret opskrift', path: '/add-recipe', icon: <BsFillPlusCircleFill /> },
  { id: 6, text: 'Mine opskrifter', path: '/all-recipes', icon: <FaClipboardList /> },
  { id: 7, text: 'Mine Favoritter', path: '/all-favorites', icon: <BsHeartFill /> },
  { id: 8, text: 'Min profile', path: '/profile', icon: <FaUserCircle /> },
]

export default links
