// import { useAppContext } from "../context/appContext";
// import StatItem from "./StatItem";
// import { FaSuitcaseRolling, FaCalendarCheck, FaBug } from "react-icons/fa";
import { Link } from "react-router-dom";
import Wrapper from "../assets/wrappers/StatsContainer";
import SmallMenuBox from "./SmallMenu";
import ditfood from "../images/food-ditkokken.jpg";
const StatsContainer = () => {
  // const { stats } = useAppContext();
  return (
    <Wrapper>
      <h1>Mad for alle</h1>
      <p>
        Dit køkken hjælper dig med at finde de bedste madopsrifter fra hele
        verden.
      </p>
      <Link to="/all-recipes" className="btn se-btn">
        Se alle opskrifter
      </Link>
      <img src={ditfood} width="350px" alt="ditkokken-food" />
      <SmallMenuBox />
    </Wrapper>
  );
};
export default StatsContainer;
