import { FormRow, FormRowSelect } from '.'
import { useAppContext } from '../context/appContext'
import Wrapper from '../assets/wrappers/SearchContainer'

const SearchContainer = () => {
  const {
    isLoading,
    search,
    searchStatus,
    searchType,
    sort,
    sortOptions,
    handleChange,
    clearFilters,
    timeIntotalOptions,
    numberOfPeopleOptions,
  } = useAppContext()
  const handleSearch = (e) => {
    if (isLoading) return
    handleChange({ name: e.target.name, value: e.target.value })
  }
  const handleSubmit = (e) => {
    e.preventDefault()
    clearFilters()
  }
  return (
    <Wrapper>
      <form className='form'>
        <div className='form-center'>
          {/* search position */}

          <FormRow
            type='text'
            labelText="søgeord"
            name='search'
            value={search}
            handleChange={handleSearch}
          />
          {/* search by status */}
          <FormRowSelect
            labelText='antal personer'
            name='searchStatus'
            value={searchStatus}
            handleChange={handleSearch}
            list={['all', ...numberOfPeopleOptions]}
          />
          {/* search by type */}
          <FormRowSelect
            labelText='Tid i alt'
            name='searchType'
            value={searchType}
            handleChange={handleSearch}
            list={['all', ...timeIntotalOptions]}
          />
          {/* sort */}
          <FormRowSelect
            name='sort'
            labelText="sortere"
            value={sort}
            handleChange={handleSearch}
            list={sortOptions}
          />
          <button
            className='btn btn-block btn-danger'
            disabled={isLoading}
            onClick={handleSubmit}
          >
            ryd filtre
          </button>
        </div>
      </form>
    </Wrapper>
  )
}

export default SearchContainer
