import logoBlack from '../assets/images/cooking_logo_black.svg'

const Logo = () => {
  return <img src={logoBlack} width="180px" height="250px" alt='Dit køkken' className='logo' />
}

export default Logo