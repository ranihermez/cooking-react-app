import { useAppContext } from '../context/appContext'
import { useEffect } from 'react'
import Loading from './Loading'
import Recipe from './Recipe'
import Wrapper from '../assets/wrappers/RecipesContainer'
import PageBtnContainer from './PageBtnContainer'

const RecipesContainer = () => {
  const {
    getRecipes,
    recipes,
    isLoading,
    page,
    totalRecipes,
    search,
    searchStatus,
    searchType,
    sort,
    numOfPages,
  } = useAppContext()
  useEffect(() => {
    getRecipes()
    // eslint-disable-next-line
  }, [page, search, searchStatus, searchType, sort])
  if (isLoading) {
    return <Loading center />
  }

  if (recipes.length === 0) {
    return (
      <Wrapper>
        <h2>Ingen opskrifter endnu...</h2>
      </Wrapper>
    )
  }
  return (
    <Wrapper>
      <h5>
        {totalRecipes} opskrift{recipes.length > 1 && 's'} fundet
      </h5>
      <div className='recipes'>
        {recipes.map((recipe) => {
          return <Recipe key={recipe._id} {...recipe} />
        })}
      </div>
      {numOfPages > 1 && <PageBtnContainer />}
    </Wrapper>
  )
}

export default RecipesContainer
