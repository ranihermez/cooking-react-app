import { Link } from "react-router-dom";
import { useAppContext } from "../context/appContext";
import Wrapper from "../assets/wrappers/Favorite";
import slideImg1 from "../images/food.jpg";
import { FaHeart } from "react-icons/fa";

const Favorite = ({ _id, recipeId, recipeTitle, userId}) => {
  const { getFavorite, deleteFavorite } = useAppContext();

  return (
    <Wrapper>
      <img
        style={{
          width: 350,
          borderRadius: 25,
          justifySelf: "center",
        }}
        src={slideImg1}
        alt={slideImg1}
      />
      <header>
        <div className="info">
          <h1>{recipeTitle}</h1>
        </div>
      </header>
      <div className="content">
        <div className="content-center">
          <Link
            to="/get-recipe"
            className="btn se-btn"
            onClick={() => getFavorite(recipeId)}
          >
            Se opskrift
          </Link>
          <button
            type="button"
            className="btn delete-btn"
            onClick={() => deleteFavorite(_id)}
          >
            <FaHeart /> Fjern favorit
          </button>
        </div>
      </div>
    </Wrapper>
  );
};

export default Favorite;
