import React, { Component } from "react";
import { FaHeart, FaHome } from "react-icons/fa";
import { BsCalendar2EventFill, BsStack} from "react-icons/bs";
import { MdRestaurantMenu } from "react-icons/md";
import {
  RectStack,
  Rect,
  Rect2,
  Flex,
  FlexWrapper,
  DisplayNone
} from "../assets/wrappers/SmallMenu";
import { Link } from "react-router-dom";

function SmallMenuBox(props) {
  const style = {
    color: "rgba(255,135,48,1)",
    fontSize: 45,
    width: 45,
    marginTop: 12,
    marginLeft: 8,
  };
  const icon = {
    color: "#676767",
    fontSize: 30,
  };
  const text = {
    color: "#03764d",
    fontFamily:"Cairo",
  };
  return (
    <DisplayNone>
    <RectStack>
      <Rect>
        <Flex>
          <FlexWrapper>
            <Link to={"/"}>
              <FaHome style={icon} />
            </Link>
            <div style={text}>Hjem</div>
          </FlexWrapper>
          <FlexWrapper>
            <Link to={"/all-recipes"}>
              <BsStack style={icon} /></Link>
            <div style={text}> Opskrifter</div>
          </FlexWrapper>
          <FlexWrapper>
            <Link to={"/"}>
              <MdRestaurantMenu style={icon} />
            </Link>
            <div style={text}>Menu</div>
          </FlexWrapper>
          <FlexWrapper>
          <Link  to={"/"}>
            <BsCalendar2EventFill style={icon} /> 
          </Link>
          <div style={text}>Madplan</div>
          </FlexWrapper>
       
        </Flex>
      </Rect>
        <Rect2>
        <Link to={"/all-favorites"}>
          <FaHeart style={style} />
          </Link>
        </Rect2>
    </RectStack>
    </DisplayNone>
  );
}

export default SmallMenuBox;
