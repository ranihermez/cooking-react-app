import { useAppContext } from "../context/appContext";
import { useEffect } from "react";
import Loading from "./Loading";
import Favorite from "./Favorite";
import Wrapper from "../assets/wrappers/FavoritesContainer";
import PageBtnContainer from "./PageBtnContainer";
import favicon from "../../src/assets/images/fav-icon.svg";

const FavoritesContainer = () => {
  const { getFavorites, favorites, isLoading, page, sort, numOfPages } =
    useAppContext();
  useEffect(() => {
    getFavorites();
    // eslint-disable-next-line
  }, [page, sort]);
  if (isLoading) {
    return <Loading center />;
  }

  if (favorites.length === 0) {
    return (
      <Wrapper>
        <h1>Mine favoritter</h1>
        <p>
          Vi kan ikke se, om du har favoritter. <br /> Men log ind, så henter vi
          dine favoritter, hvis du tidlligere har gemt nogen.
        </p>
        <img src={favicon} alt="favicon" />
      </Wrapper>
    );
  }
  return (
    <Wrapper>
      <div className="recipes">
        {favorites.map((favorite) => {
          return <Favorite key={favorite._recipeId} {...favorite} />;
        })}
      </div>
      {numOfPages > 1 && <PageBtnContainer />}
    </Wrapper>
  );
};

export default FavoritesContainer;
