// import { FaLocationArrow, FaBriefcase, FaCalendarAlt } from "react-icons/fa";
import { Link } from "react-router-dom";
import { useAppContext } from "../context/appContext";
import Wrapper from "../assets/wrappers/Recipe";
// import RecipeInfo from "./RecipeInfo";
import slideImg1 from '../images/food.jpg';
const Recipe = ({
  _id,
  title
}) => {
  const { setEditRecipe, deleteRecipe, getRecipe } = useAppContext();

  return (
    <Wrapper> 
      <img  style={{
            width: 350,
            borderRadius:25,
            justifySelf:"center"
          }} src={slideImg1} alt={slideImg1}/>
      <header>
       
        <div className="info">
          <h1>{title}</h1>
        </div>
      </header>
      <div className="content">
        <div className="content-center">
        <Link
              to="/get-recipe"
              className="btn se-btn"
              onClick={() => getRecipe(_id)}
            >
               Se opskrift
            </Link>
        </div>
        <footer>
          <div className="actions">
            <Link
              to="/add-recipe"
              className="btn edit-btn"
              onClick={() => setEditRecipe(_id)}
            >
              Redigere
            </Link>
            <button
              type="button"
              className="btn delete-btn"
              onClick={() => deleteRecipe(_id)}
            >
              Slet
            </button>
          </div>
        </footer>
      </div>
    </Wrapper>
  );
};

export default Recipe;
