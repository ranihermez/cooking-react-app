import logo from '../assets/images/cooking_logo.svg'

const Logo = () => {
  return <img src={logo} alt='Dit køkken' className='logo' />
}

export default Logo
