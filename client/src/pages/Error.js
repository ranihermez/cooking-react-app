import { Link } from 'react-router-dom'
import img from '../assets/images/not-found.svg'
import Wrapper from '../assets/wrappers/ErrorPage'

const Error = () => {
  return (
    <Wrapper className='full-page'>
      <div>
        <img src={img} alt='not found' />
        <p>Vi kan tilsyneladende ikke finde den side, du leder efter</p>
        <Link to='/'>Tilbage til forsiden</Link>
      </div>
    </Wrapper>
  )
}

export default Error
