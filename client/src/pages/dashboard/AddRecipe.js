import { FormRow, FormRowSelect, Alert } from "../../components";
import { useAppContext } from "../../context/appContext";
import Wrapper from "../../assets/wrappers/DashboardFormPage";

const AddRecipe = () => {
  const {
    isLoading,
    isEditing,
    showAlert,
    displayAlert,
    title,
    workingHours,
    workingHoursOptions,
    ingredients,
    courseOfAction,
    imageUrl,
    timeIntotal,
    timeIntotalOptions,
    numberOfPeople,
    numberOfPeopleOptions,
    handleChange,
    clearValues,
    createRecipe,
    editRecipe,
  } = useAppContext();

  const handleSubmit = (e) => {
    e.preventDefault();

    if (
      !title ||
      !timeIntotal ||
      !workingHours ||
      !numberOfPeople ||
      !ingredients ||
      !courseOfAction ||
      !imageUrl
    ) {
      displayAlert();
      return;
    }
    if (isEditing) {
      editRecipe();
      return;
    }
    createRecipe();
  };

  const handleRecipeInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    handleChange({ name, value });
  };

  return (
    <Wrapper>
      <form className="form" encType="multipart/form-data">
        <h3>{isEditing ? "redigere opskrift" : "opret opskrift"}</h3>
        {showAlert && <Alert />}
        <div className="form-center">
          <FormRow
            type="text"
            labelText="Titel på opskriften"
            name="title"
            value={title}
            handleChange={handleRecipeInput}
          />

          <FormRowSelect
            name="timeIntotal"
            labelText="Tid i alt"
            handleChange={handleRecipeInput}
            list={timeIntotalOptions}
          />
          <FormRowSelect
            type="text"
            name="workingHours"
            labelText="Arbejdestid"
            value={workingHours}
            handleChange={handleRecipeInput}
            list={workingHoursOptions}
          />
          <FormRowSelect
            name="numberOfPeople"
            labelText="Antal personer"
            value={numberOfPeople}
            handleChange={handleRecipeInput}
            list={numberOfPeopleOptions}
          />
          {
            <FormRow
              type="text"
              name="ingredients"
              labelText="Ingredienser"
              value={ingredients}
              handleChange={handleRecipeInput}
            />
          }
          <FormRow
            type="text"
            name="courseOfAction"
            labelText="Fremgangsmåde"
            value={courseOfAction}
            handleChange={handleRecipeInput}
          />
          <FormRow
            type="file"
            labelText="Billede"
            name="imageUrl"
            handleChange={handleRecipeInput}
          />
          {/* btn container */}
          <div className="btn-container">
            <button
              type="submit"
              className="btn btn-block submit-btn"
              onClick={handleSubmit}
              disabled={isLoading}
            >
              {isEditing ? "update" : "opret"}
            </button>
            <button
              className="btn btn-block clear-btn"
              onClick={(e) => {
                e.preventDefault();
                clearValues();
              }}
            >
              ryd alt
            </button>
          </div>
        </div>
      </form>
    </Wrapper>
  );
};

export default AddRecipe;
