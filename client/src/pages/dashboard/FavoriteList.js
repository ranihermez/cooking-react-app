import { FavoritesContainer, SmallMenu } from "../../components";
import Wrapper from "../../assets/wrappers/FavoritesList";
const FavoriteList = () => {
  return (
    <Wrapper>
    <h1>Alle dine favoriter</h1>
      <FavoritesContainer />
      <SmallMenu />
    </Wrapper>
  );
};

export default FavoriteList;
