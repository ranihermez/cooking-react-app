import { useAppContext } from "../../context/appContext";
import Wrapper from "../../assets/wrappers/Recipe";
import slideImg1 from "../../images/food.jpg";
import { Link } from "react-router-dom";
import { AiFillPrinter } from "react-icons/ai";
import { FaHeart } from "react-icons/fa";
import { SmallMenu, Alert } from "../../components";

const GetOneRecipe = () => {
  const {
    isLoading,
    showAlert,
    title,
    workingHours,
    ingredients,
    courseOfAction,
    timeIntotal,
    numberOfPeople,
    createFavorite,
  } = useAppContext();
  const handleSubmit = (e) => {
    e.preventDefault();
    createFavorite();
  };
  return (
    <Wrapper>
      <img
        style={{
          width: 350,
          borderRadius: 25,
          justifySelf: "center",
        }}
        src={slideImg1}
        alt={slideImg1}
      />
      <header>
        <div className="info">
          <h1>{title}</h1>
          <div className="se-recipe">
            <p className="se-p">Tid i alt: {timeIntotal}</p>
            <p className="se-p"> Arbejdestid: {workingHours}</p>
            <p className="se-p">Antal: {numberOfPeople} pers.</p>
          </div>
          <div className="ing-se">
            <div>
              <p>Ingredienser</p>
              <hr></hr>
              <p className="ing-se-des">{ingredients}</p>
            </div>
            <div>
              <p>Fremgangsmåde</p>
              <hr></hr>
              <p className="ing-se-des"> {courseOfAction}</p>
            </div>
          </div>
        </div>
      </header>
      <div className="content">
        <div className="content-center">
          <button
            type="submit"
            className="btn se-btn"
            onClick={handleSubmit}
            disabled={isLoading}
          >
            <FaHeart className="icon" /> Favorit
          </button>
          <Link to="/get-recipe" className="btn se-btn">
            <AiFillPrinter className="icon" /> Print
          </Link>
        </div>
        {showAlert && <Alert />}
      </div>
      
      <SmallMenu />
    </Wrapper>
  );
};

export default GetOneRecipe;
