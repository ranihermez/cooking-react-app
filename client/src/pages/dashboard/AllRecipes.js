import { RecipesContainer, SmallMenu, SearchContainer } from "../../components";

const AllRecipes = () => {
  return (
    <>
      <SearchContainer />
      <RecipesContainer />
      <SmallMenu />
    </>
  );
};

export default AllRecipes;
