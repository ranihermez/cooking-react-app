import Wrapper from '../assets/wrappers/LandingPage'
import { LogoBlack } from '../components'
import { Link } from 'react-router-dom'
import { FaFacebookF } from "react-icons/fa";
import { FcGoogle } from "react-icons/fc";
const Landing = () => {
  return (
    <Wrapper>
      <nav>
        <LogoBlack />
      </nav>
      <div className='container page'>
        <div className='info'>
          <p>
            Log ind med
          </p>
          <Link to='/register' className='btn btn-facebook'>
          <FaFacebookF /> Log ind med Facebook
          </Link>
          <Link to='/register' className='btn btn-google'>
          <FcGoogle /> Log ind med Google
          </Link>
          <p>eller</p>
          <Link to='/register' className='btn btn-hero'>
          Log ind eller Register
          </Link>
     
        </div>
      </div>
    </Wrapper>
  )
}

export default Landing
