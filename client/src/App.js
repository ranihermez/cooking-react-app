import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Register, Landing, Error, ProtectedRoute } from "./pages";
import {
  AllRecipes,
  Profile,
  SharedLayout,
  Stats,
  AddRecipe,
  GetOneRecipe,
  FavoriteList,
} from "./pages/dashboard";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <ProtectedRoute>
              <SharedLayout />
            </ProtectedRoute>
          }
        >
          <Route index element={<Stats />} />
          <Route path="all-recipes" element={<AllRecipes />} />
          <Route path="get-recipe" element={<GetOneRecipe />} />
          <Route path="all-favorites" element={<FavoriteList />} />
          <Route path="add-recipe" element={<AddRecipe />} />
          <Route path="profile" element={<Profile />} />
        </Route>
        <Route path="/register" element={<Register />} />
        <Route path="/landing" element={<Landing />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
