const puppeteer = require("puppeteer");

test("should login", async () => {
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 100,
  });
  const page = await browser.newPage();

  // navigere til denne login side

  await page.goto("http://localhost:3000/register");

  // trykker på email felt og derefter indtaster email = rani08@live.com
  await page.click("input[name=email]");
  await page.type("input[name=email]", "rani09@live.com");

  // trykker på password felt og derefter indtaster email = rani08@live.com

  await page.click("input[name=password]");
  await page.type("input[name=password]", "rani1992");

  // tryker på opret knappen for at genennemføre processen
  await page.click(".btn-block");

  await page.waitForSelector(".form-success-message");

  const text = await page.$eval(".form-success-message", (e) => e.textContent);
  expect(text).toContain("Login Successful! Redirecting...");

  await browser.close();
}, 100000);