import renderer from "react-test-renderer";
import { RecipesContainer } from "../components";
import { AppProvider } from "../context/appContext";

it("should render list of recipes", async () => {
  const tree = renderer.create(
    <AppProvider>
    <RecipesContainer />
  </AppProvider>
  );
  expect(tree).toMatchSnapshot();
});
