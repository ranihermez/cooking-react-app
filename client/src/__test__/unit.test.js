import React from "react";
import { render, cleanup } from "@testing-library/react";
import { RecipesContainer } from "../components";
import { AppProvider } from "../context/appContext";
// afterEach function runs after each test suite is executed
afterEach(() => {
  cleanup();
});

test("renders recipesContainer component", async () => {
  const { container } = render(
    <AppProvider>
      <RecipesContainer />
    </AppProvider>
  );
  const recipes = container.getElementsByClassName("article");
  expect(recipes).toBe(recipes);
});
