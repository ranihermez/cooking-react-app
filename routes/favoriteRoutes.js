import express from 'express'
const router = express.Router()

import {
createFavorite,
getAllFavorites,
deleteFavorite
} from '../controllers/favoriteController.js'

router.route('/').post(createFavorite).get(getAllFavorites)
router.route('/:id').delete(deleteFavorite)

export default router